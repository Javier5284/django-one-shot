from django.shortcuts import render, get_object_or_404
from todos.models import TodoList

# Create your views here.


def list_view(request):
    to_do_lists = TodoList.objects.all()
    context = {
        "to_do_lists": to_do_lists,
    }
    return render(request, "todos/todos.html", context)


def list_details(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": list_detail,
    }
    return render(request, "todos/todo_list_detail.html", context)
