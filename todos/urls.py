from django.urls import path
from todos.views import list_view, list_details


urlpatterns = [
    path("", list_view, name="todo_list_list"),
    path("<int:id>/", list_details, name="list_details"),
]
